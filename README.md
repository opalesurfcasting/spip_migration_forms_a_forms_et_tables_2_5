# README #

Modification du plugin SPIP [forms_et_tables_2_5](https://contrib.spip.net/Forms-Table-2-5)

###Pour quoi faire ? ###

* Migrer des donn�es issues du plugin [Forms](https://contrib.spip.net/plugin-Forms-creation-de) (depuis SPIP 1.8.3)  vers [forms_et_tables_2_5](https://contrib.spip.net/Forms-Table-2-5) associ� � SPIP 3.1.x
* Ce qui permet ensuite de migrer les donn�es  vers le plugin actuel : [Formidable](https://contrib.spip.net/Formidable-le-generateur-de-formulaires)